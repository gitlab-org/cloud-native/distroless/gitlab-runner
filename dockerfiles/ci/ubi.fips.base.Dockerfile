ARG UBI_VERSION=8.9
ARG UBI_MICRO_IMAGE=registry.access.redhat.com/ubi8/ubi-micro
ARG UBI_MINIMAL_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal


FROM ${UBI_MICRO_IMAGE}:${UBI_VERSION} as initial

FROM ${UBI_MINIMAL_IMAGE}:${UBI_VERSION} as build

ARG DNF_OPTS_ROOT="--installroot=/install-root/ --noplugins  --setopt=reposdir=/install-root/etc/yum.repos.d/ \
    --setopt=varsdir=/install-var/ --config=/install-root/etc/yum.repos.d/ubi.repo --setopt=cachedir=/install-cache/"

RUN mkdir -p /install-root/ /install-var
COPY --from=initial / /install-root/ 


# these packages are required by downstream images, but we don't know anymore specifically which images require which
# packages, so we'll install them all here...
RUN microdnf update ${DNF_OPTS_ROOT}  --best --refresh --assumeyes --nodocs --setopt=install_weak_deps=0 --setopt=tsflags=nodocs \
    && microdnf install ${DNF_OPTS_ROOT} --best --refresh  --assumeyes --nodocs  --setopt=install_weak_deps=0 --setopt=tsflags=nodocs \
    hostname procps tar gzip ca-certificates tzdata openssl shadow-utils git git-lfs findutils \
    && microdnf clean  ${DNF_OPTS_ROOT}  all \
    && rm -f /install-root/var/lib/dnf/history* 
    
FROM ${UBI_MICRO_IMAGE}:${UBI_VERSION}

COPY --from=build  /install-root/ /
RUN git-lfs install --skip-repo \
    && rm -f /var/lib/dnf/history*
